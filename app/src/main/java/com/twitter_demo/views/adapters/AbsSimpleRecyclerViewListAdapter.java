package com.twitter_demo.views.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Hanan on 20/01/2016.
 */
public abstract class AbsSimpleRecyclerViewListAdapter<ItemType, ViewHolderType extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<ViewHolderType> {

    protected Handler mHandler = new Handler(Looper.getMainLooper());

    protected List<ItemType> mItems;

    public AbsSimpleRecyclerViewListAdapter()
    {
        mItems = new ArrayList<>();
    }

    /**
     * Updates adapter items, returns true if list was updated
     *
     * @param items
     * @return
     */
    public boolean updateData(List<? extends ItemType> items)
    {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
        return true;
    }


    public void sortData(Comparator<ItemType> comparator)
    {
        Collections.sort(mItems, comparator);
        notifyDataSetChanged();
    }

    public boolean addItem(ItemType item)
    {
        mItems.add(item);
        notifyItemInserted(getItemCount() - 1);
        return true;
    }

    public boolean addItems(Collection<ItemType> items)
    {
        int size = getItemCount();
        mItems.addAll(items);
        notifyItemRangeInserted(size, items.size());
        return true;
    }

    /**
     * Make sure you override equals() for the item type before using this method
     *
     * @param item
     */
    public void removeItem(ItemType item)
    {
        removeItem(mItems.indexOf(item));
    }

    public void removeItem(int indexOfItem)
    {
        if(indexOfItem >= 0 && indexOfItem < mItems.size())
        {
            mItems.remove(indexOfItem);
            notifyItemRemoved(indexOfItem);
        }
    }

    public void removeFirst()
    {
        if(!mItems.isEmpty())
        {
            removeItem(0);
        }
    }

    public void removeLast()
    {
        if(!mItems.isEmpty())
        {
            removeItem(mItems.size() - 1);
        }
    }


    /**
     * Make sure you override equals() for the item type before using this method
     *
     * @param newItem
     */
    public void updateItem(ItemType newItem)
    {
        int indexOfItem = mItems.indexOf(newItem);
        if(indexOfItem > 0)
        {
            mItems.set(indexOfItem, newItem);
            notifyItemChanged(indexOfItem);
        }
    }


    @Override
    public int getItemCount()
    {
        return mItems.size();
    }

    public ItemType getItemAt(int position)
    {
        return position < mItems.size() && position >= 0 ? mItems.get(position) : null;
    }


    public void clear()
    {
        mItems.clear();
        notifyDataSetChanged();
    }

    public List<ItemType> getItems()
    {
        return new ArrayList<>(mItems);
    }

    @Override
    public int getItemViewType(int position)
    {
        return getItemType(getItemAt(position));
    }

    protected abstract int getItemType(ItemType item);

    @Override
    public final void onBindViewHolder(ViewHolderType holder, int position)
    {
        onBindViewHolder(holder, getItemAt(position), getItemViewType(position), position);
    }

    protected abstract void onBindViewHolder(ViewHolderType holder, ItemType item, int type, int position);


}
