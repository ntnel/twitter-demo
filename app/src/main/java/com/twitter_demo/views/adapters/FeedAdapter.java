// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package com.twitter_demo.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twitter_demo.android_interview.R;
import com.twitter_demo.managers.ImageManager;

import twitter4j.Status;

public final class FeedAdapter extends AbsSimpleRecyclerViewListAdapter<Status, FeedAdapter.ViewHolder> {

    private IOnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(IOnItemClickListener onItemClickListener)
    {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    protected int getItemType(Status item)
    {
        return 0;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType)
    {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (mOnItemClickListener != null)
                {
                    mOnItemClickListener.onItemClicked(getItemAt(viewHolder.getAdapterPosition()));
                }
            }
        });
        return viewHolder;
    }

    @Override
    protected void onBindViewHolder(ViewHolder holder, Status item, int type, int position)
    {
        holder.mAuthor.setText(item.getUser().getName());
        holder.mTweetText.setText(item.getText());
        ImageManager.getInstance().load(item.getUser().getProfileImageURL(), holder.mAvatar);
        if (item.getExtendedMediaEntities().length > 0)
        {
            holder.mTweetImage.setVisibility(View.VISIBLE);
            ImageManager.getInstance().load(item.getExtendedMediaEntities()[0].getMediaURL(), holder.mTweetImage);
        }
        else
        {
            holder.mTweetImage.setImageDrawable(null);
            holder.mTweetImage.setVisibility(View.GONE);
        }
    }

    //region: Inner class: ViewHolder

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    static final class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mAuthor;
        private final TextView mTweetText;
        private final ImageView mAvatar;
        private final ImageView mTweetImage;


        public ViewHolder(View v)
        {
            super(v);
            mAuthor = (TextView) v.findViewById(R.id.author);
            mTweetText = (TextView) v.findViewById(R.id.tweet_text);
            mAvatar = (ImageView) v.findViewById(R.id.avatar);
            mTweetImage = (ImageView) v.findViewById(R.id.tweet_img);

        }
    }
    //endregion

    public interface IOnItemClickListener {
        void onItemClicked(Status item);
    }
}

