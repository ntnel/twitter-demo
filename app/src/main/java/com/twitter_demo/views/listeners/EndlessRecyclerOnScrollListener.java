package com.twitter_demo.views.listeners;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    private static final int VISIBLE_THRESHOLD = 1; // The minimum amount of items to have below your current scroll position before mIsLoading more.

    private int mPreviousTotal = 0; // The total number of items in the dataset after the last load
    private boolean mIsLoading = true; // True if we are still waiting for the last set of data to load.
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;

    private int mFirstPage;
    private int mCurrentPage;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessRecyclerOnScrollListener(int firstPage, LinearLayoutManager linearLayoutManager)
    {
        this.mLinearLayoutManager = linearLayoutManager;
        mFirstPage = firstPage;
        reset();
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy)
    {
        super.onScrolled(recyclerView, dx, dy);

        mVisibleItemCount = recyclerView.getChildCount();
        mTotalItemCount = mLinearLayoutManager.getItemCount();
        mFirstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (mIsLoading)
        {
            if (mTotalItemCount > mPreviousTotal)
            {
                mIsLoading = false;
                mPreviousTotal = mTotalItemCount;
            }
        }
        if (!mIsLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + VISIBLE_THRESHOLD))
        {
            // End has been reached

            // Do something
            mCurrentPage++;

            onLoadMore(mCurrentPage);

            mIsLoading = true;
        }
    }

    public void reset()
    {
        mPreviousTotal = 0;
        mCurrentPage = mFirstPage;
    }

    protected abstract void onLoadMore(int currentPage);
}