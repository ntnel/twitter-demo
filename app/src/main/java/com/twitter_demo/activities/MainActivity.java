// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package com.twitter_demo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.twitter_demo.android_interview.R;
import com.twitter_demo.network.TwitterServiceManager;
import com.twitter_demo.network.callbacks.ICallback;
import com.twitter_demo.utils.CaptureImageHelper;
import com.twitter_demo.utils.IntentFactory;
import com.twitter_demo.utils.PermissionsUtils;
import com.twitter_demo.utils.Toaster;
import com.twitter_demo.views.adapters.FeedAdapter;
import com.twitter_demo.views.listeners.EndlessRecyclerOnScrollListener;

import twitter4j.ResponseList;
import twitter4j.Status;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, FeedAdapter.IOnItemClickListener {

    private static final String LOG_TAG = "MainActivity";

    private static final int IMAGE_SELECT_FOR_TWEET_CODE = 1;

    private static final int FIRST_PAGE = 1;

    private int mImageSelectCode;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EndlessRecyclerOnScrollListener mEndlessScrollListener;
    private EditText mPostEditText;
    private FeedAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_view);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initFeedRecyclerViewAndLoadData();

        initLowerBar();
    }


    private void initLowerBar()
    {
        mPostEditText = (EditText) findViewById(R.id.edittext_post);
        findViewById(R.id.btn_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                onPostButtonClicked();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private String getPostText()
    {
        String text = mPostEditText.getText().toString();
        if (text.isEmpty())
        {
            text = getString(R.string.base_tweet_text);
        }
        return text;
    }

    private void initFeedRecyclerViewAndLoadData()
    {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, layoutManager.getOrientation()));
        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(FIRST_PAGE, layoutManager) {

            @Override
            protected void onLoadMore(int currentPage)
            {
                loadData(currentPage);
            }
        };
        enableEndlessScroll();

        mAdapter = new FeedAdapter();
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);

        loadData();
    }

    private void enableEndlessScroll()
    {
        mRecyclerView.addOnScrollListener(mEndlessScrollListener);
    }

    private void disableEndlessScroll()
    {
        mRecyclerView.removeOnScrollListener(mEndlessScrollListener);
    }

    private void onPostButtonClicked()
    {
        mImageSelectCode = IMAGE_SELECT_FOR_TWEET_CODE;
        if (!PermissionsUtils.assertStoragePermission(this))
        {
            return;
        }
        CaptureImageHelper.selectImage(this);
    }

    @Override
    public void onRefresh()
    {
        mEndlessScrollListener.reset();
        enableEndlessScroll();
        loadData();
    }

    private void loadData()
    {
        loadData(1);
    }

    private void loadData(final int page)
    {
        Log.d(LOG_TAG, "loadData() called with: page = [" + page + "]");
        mSwipeRefreshLayout.setRefreshing(true);
        TwitterServiceManager.getInstance().getFeed(page, new ICallback<ResponseList<Status>>() {
            @Override
            public void success(ResponseList<Status> feed)
            {
                Log.d(LOG_TAG, "loadData().success() ");
                onFeedLoaded(feed, page);
            }

            @Override
            public void failed(Exception error)
            {
                Log.d(LOG_TAG, "loadData().fail() ");
                mSwipeRefreshLayout.setRefreshing(false);
                Toaster.show(getString(R.string.toast_feed_load_fail, error.getMessage()));
            }
        });
    }

    private void onFeedLoaded(ResponseList<Status> feed, int page)
    {
        mSwipeRefreshLayout.setRefreshing(false);

        //Check if more available
        if (feed.getRateLimitStatus().getRemaining() < 1)
        {
            disableEndlessScroll();
        }
        if (page > FIRST_PAGE)
        {
            mAdapter.addItems(feed);
        }
        else
        {
            mAdapter.updateData(feed);
        }
        Toaster.show(getResources().getQuantityString(R.plurals.toast_feed_load_success, feed.size(), feed.size()));
    }

    @Override
    public void onItemClicked(Status item)
    {
        Log.d(LOG_TAG, "onItemClicked() ");
        startActivity(IntentFactory.createUrlViewIntent(getString(R.string.tweet_url_format, item.getId())));
    }

    private void onImageSelected(String imagePath)
    {
        Log.d(LOG_TAG, "onImageSelected() ");
        if (imagePath == null)
        {
            Toaster.showShort(R.string.toast_image_select_fail);
            return;
        }
        switch (mImageSelectCode)
        {
            case IMAGE_SELECT_FOR_TWEET_CODE:
                postImageTweet(getPostText(), imagePath);
                break;
        }
    }

    private void postImageTweet(String postText, String imagePath)
    {
        Log.d(LOG_TAG, "postImageTweet() called with: postText = [" + postText + "], imagePath = [" + imagePath + "]");
        mPostEditText.setText("");
        Toaster.showShort(getString(R.string.toast_posting_status));
        TwitterServiceManager.getInstance().postImageTweet(imagePath, postText);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case CaptureImageHelper.IMAGE_SELECT_REQUEST_CODE:
                    onImageSelected(CaptureImageHelper.getPath(this, data));
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean permissionGranted = PermissionsUtils.isPermissionGranted(grantResults);
        switch (requestCode)
        {
            case PermissionsUtils.EXTERNAL_STORAGE_READ_REQUEST_CODE:
                onStoragePermissionRequestResult(permissionGranted);
                break;
        }
    }

    private void onStoragePermissionRequestResult(boolean permissionGranted)
    {
        Log.d(LOG_TAG, "onStoragePermissionRequestResult() called with: permissionGranted = [" + permissionGranted + "]");
        if (permissionGranted)
        {
            CaptureImageHelper.selectImage(this);
        }
        else
        {
            Toaster.show(R.string.toast_storage_permission_denied);
        }
    }
}
