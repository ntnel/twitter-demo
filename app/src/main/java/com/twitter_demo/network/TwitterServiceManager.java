// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package com.twitter_demo.network;

import android.os.Handler;
import android.util.Log;

import com.twitter_demo.network.callbacks.ICallback;

import java.io.File;

import twitter4j.AsyncTwitter;
import twitter4j.AsyncTwitterFactory;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterAdapter;
import twitter4j.TwitterException;
import twitter4j.TwitterMethod;
import twitter4j.auth.AccessToken;

public class TwitterServiceManager extends TwitterAdapter {

    private static final String LOG_TAG = "TwitterServiceManager";

    private static final String CONSUMER_KEY = "f0Et7h78uXUPfr0Tj44h9vZpL";
    private static final String CONSUMER_SECRET = "JvU1352PlQTKg2seMTNoNrMxaWnGFLjuNXXoftwCGLSVz6Oi05";
    private static final String TOKEN = "798472472991514625-y35zX2yoSAJbHRompgXjEShAPWy9j0a";
    private static final String TOKEN_SECRET = "zEUHVkoHaKhvVrPdFL6Hdn7hgnny5Mask5UJfGbaTXzmP";

    private static TwitterServiceManager msInstance;

    public static TwitterServiceManager getInstance()
    {
        if (msInstance == null)
        {
            AsyncTwitter twitter = (new AsyncTwitterFactory()).getInstance();
            twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
            twitter.setOAuthAccessToken(new AccessToken(TOKEN, TOKEN_SECRET));
            msInstance = new TwitterServiceManager(twitter);
        }
        return msInstance;
    }

    private final AsyncTwitter mTwitter;

    private final Handler mHandler;

    private ICallback mLastCallback;


    private TwitterServiceManager(AsyncTwitter twitter)
    {
        mTwitter = twitter;
        mTwitter.addListener(this);

        mHandler = new Handler();
    }

    public void getFeed(int page, ICallback<ResponseList<Status>> callback)
    {
        mLastCallback = callback;
        mTwitter.getHomeTimeline(new Paging(page));
    }

    public void postImageTweet(String imagePath, String text)
    {
        Log.d(LOG_TAG, "postImageTweet() ");
        StatusUpdate statusUpdate = new StatusUpdate(text);
        statusUpdate.setMedia(new File(imagePath));
        mTwitter.updateStatus(statusUpdate);
    }

    @Override
    public void updatedStatus(Status status)
    {
        super.updatedStatus(status);
        Log.d(LOG_TAG, "updatedStatus() ");
    }

    @Override
    public void gotHomeTimeline(final ResponseList<Status> statuses)
    {
        Log.d(LOG_TAG, "gotHomeTimeline() ");
        mHandler.post(new Runnable() {
            @Override
            public void run()
            {
                mLastCallback.success(statuses);
            }
        });
    }

    @Override
    public void onException(final TwitterException te, TwitterMethod method)
    {
        Log.d(LOG_TAG, "onException() " + te.getMessage());
        mHandler.post(new Runnable() {
            @Override
            public void run()
            {
                mLastCallback.failed(te);
            }
        });
    }
}