// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package com.twitter_demo;

import android.app.Application;
import android.content.Context;

import com.twitter_demo.managers.ImageManager;

public final class AppApplication extends Application {


    private static Context msAppContext;

    @Override
    public void onCreate()
    {
        super.onCreate();
        msAppContext = getApplicationContext();
        ImageManager.initInstance(msAppContext);
    }

    public static Context getAppContext()
    {
        return msAppContext;
    }
}