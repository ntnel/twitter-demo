// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package com.twitter_demo.managers;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public final class ImageManager {

    private static ImageManager msInstance;

    public static ImageManager initInstance(Context context)
    {
        msInstance = new ImageManager(context);
        return msInstance;
    }

    public static ImageManager getInstance()
    {
        return msInstance;
    }

    private final Picasso mPicasso;

    public ImageManager(Context context)
    {
        mPicasso = Picasso.with(context);
    }

    public void load(String url, ImageView imageView)
    {
        load(url, imageView, null);
    }

    public void load(String url, ImageView imageView, Callback callback)
    {
        mPicasso.load(url)
                .into(imageView, callback);
    }
}

