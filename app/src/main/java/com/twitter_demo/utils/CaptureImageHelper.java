package com.twitter_demo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class CaptureImageHelper {

    public final static int IMAGE_SELECT_REQUEST_CODE = 15;

    public static void selectImage(Activity activity)
    {
        activity.startActivityForResult(IntentFactory.createImageSelectIntent(activity), IMAGE_SELECT_REQUEST_CODE);
    }

    public static String getPath(Context context, Intent data)
    {
        final boolean isCamera = isFromCamera(data);

        Uri selectedImageUri;
        if (isCamera)
        {
            selectedImageUri = (Uri) data.getExtras().get("data");
        }
        else
        {
            selectedImageUri = data == null ? null : data.getData();
        }

        return isCamera ? selectedImageUri != null ? selectedImageUri.getPath() : null : UriUtils.getPathFromURI(context, selectedImageUri);
    }


    public static boolean isFromCamera(Intent data)
    {
        boolean isCamera = false;
        if (data == null || data.getData() == null)
        {
            isCamera = true;
        }
        else
        {
            final String action = data.getAction();
            if (action == null)
            {
                isCamera = false;
            }
            else
            {
                isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            }
        }
        return isCamera;
    }
}
