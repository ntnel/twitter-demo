package com.twitter_demo.utils;

import android.widget.Toast;

import com.twitter_demo.AppApplication;

public class Toaster {


    public static void show(int textResourceId)
    {
        show(AppApplication.getAppContext().getString(textResourceId));
    }

    public static void show(String text)
    {
        show(text, Toast.LENGTH_LONG);
    }

    public static void showShort(int textResourceId)
    {
        showShort(AppApplication.getAppContext().getString(textResourceId));
    }

    public static void showShort(String text)
    {
        show(text, Toast.LENGTH_SHORT);
    }

    private static void show(String text, int length)
    {
        Toast.makeText(AppApplication.getAppContext(), text, length).show();
    }


}
