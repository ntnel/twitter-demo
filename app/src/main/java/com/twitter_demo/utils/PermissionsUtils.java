package com.twitter_demo.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Hanan on 11/15/2016.
 */

public class PermissionsUtils {

    public static final int EXTERNAL_STORAGE_READ_REQUEST_CODE = 0;

    public static boolean assertStoragePermission(Activity activity)
    {
        return assertPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                EXTERNAL_STORAGE_READ_REQUEST_CODE, activity);
    }

    private static boolean assertPermission(String permission, int requestCode, Activity activity)
    {
        boolean hasPermission = hasPermission(permission, activity);
        if (!hasPermission)
        {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
        }
        return hasPermission;
    }

    private static boolean hasPermission(String permission, Context context)
    {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isPermissionGranted(int[] grantResult)
    {
        return grantResult.length > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED;
    }
}
